package com.hexlab.request_permissions;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.hexlab.request_permissions.binding.Binding;
import com.hexlab.request_permissions.databinding.TipLayoutBinding;

/**
 * Created by oreste on 28/03/15.
 *
 * This Activity shows a tip text overlay over the Accessibility page in order to guide the user to enabling the appropriate Accessibility Service
 *
 */
public class TipActivity extends Activity {

    private TipLayoutBinding tipLayoutBinding;
    private Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.top_to_bottom, R.anim.bottom_to_top);
        super.onCreate(savedInstanceState);
        tipLayoutBinding = DataBindingUtil.setContentView(this, R.layout.tip_layout);

        tipLayoutBinding.setBinding(binding = new Binding(this));

        binding.text.set(getString(getIntent().getIntExtra("text", 0)));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.top_to_bottom, R.anim.bottom_to_top);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tipLayoutBinding.setBinding(null);
        tipLayoutBinding.unbind();
    }
}
