package com.hexlab.request_permissions;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by oreste on 29/05/15.
 *
 * Activity which launch the system request permission dialog, if the user doesn't give the permission it will iterate over the request dialog
 *
 */
public class RequestPermissionActivity extends Activity {

    public static final String PERMISSIONS_ARRAY = String.valueOf(new char[]{'p','e','r','m','i','s','s','i','o','n','s'}), IS_PERMISSION_GRANTED = "is_permission_granted",
            ACTION_PERMISSIONS_GRANTING = "com.hexlab.requestpermissions.action_permission_granting", IS_GRANTED_EXTRA = "is_granted_extra";
    private static final String EXTRA_APPLICATION_ID = "extra_application_id";
    private static final int REQUEST_CODE = 1001;
    private static ArrayList<MaterialDialog> materialDialogArrayList;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout);

        ActivityCompat.requestPermissions(this, getIntent().getStringArrayExtra(PERMISSIONS_ARRAY), REQUEST_CODE);
    }

    /**
     * handles the deny or allow selection the user has made for every permission passed in a String array
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case REQUEST_CODE:
                int indexGranted = 0;
                for(int result : grantResults){
                    if(result==PackageManager.PERMISSION_GRANTED){
                        indexGranted++;
                    }
                }
                finish();
                boolean isGranted;
                if(isGranted = (indexGranted==grantResults.length)){
                    destroy();
                }
                sendBroadcast(new Intent(ACTION_PERMISSIONS_GRANTING).putExtra(IS_GRANTED_EXTRA, isGranted),
                        getIntent().getStringExtra(EXTRA_APPLICATION_ID)+getString(R.string.permission_permision_not_granted)
                );
        }

    }

    /**
     * destroys arraylist of MaterialDialogs
     */
    private void destroy() {
        if(materialDialogArrayList!=null){
            materialDialogArrayList.clear();
            materialDialogArrayList = null;
        }
    }

    /**
     * launches a settings activity by passing a tip text and the related settings action to launch
     *
     * @param context
     * @param action
     * @param uriPackage
     * @param text
     */
    public static void launchSettingsActivity(final Context context, String action, Uri uriPackage, @StringRes final int text){
        Intent intent = new Intent(action).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if(uriPackage!=null){
            intent.setData(uriPackage);
        }
        context.startActivity(intent);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                context.startActivity(new Intent(context, TipActivity.class).putExtra("text", text).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        }, TimeUnit.MILLISECONDS.toMillis(500));
    }

    /**
     *
     * @param context
     * @param shouldShowCustomRationaleDialog
     * @param appId
     * @param permissions
     * @return true if the fingerprint permission is granted, false otherwise and starts the activity which warns the user to activate it
     */
    public static Bundle isPermissionGranted(Context context, boolean shouldShowCustomRationaleDialog, String appId, String... permissions){
        return isPermissionGranted(null, context, null, shouldShowCustomRationaleDialog, appId, permissions);
    }

    /**
     * @param activity
     * @param context
     * @param ratinaleStrings
     * @param shouldShowCustomRationaleDialog
     * @param appId
     * @param permissions
     * @return true if the fingerprint permission is granted, false otherwise and starts the activity which warns the user to activate it
     */
    @TargetApi(Build.VERSION_CODES.M)
    public static Bundle isPermissionGranted(final AppCompatActivity activity, Context context, String[] ratinaleStrings, boolean shouldShowCustomRationaleDialog , String appId, String... permissions){
        Bundle bundle = new Bundle();
        if(!isAndroidM()){
            bundle.putBoolean(IS_PERMISSION_GRANTED,true);
            return bundle;
        }
        ArrayList<String> stringArrayList =new ArrayList<String>();
        int index = 0;
        boolean isRationaleLaunched = false;
        for(String permission : permissions){
            if(ContextCompat.checkSelfPermission(context,permission)==PackageManager.PERMISSION_DENIED){
                if(activity!=null && ActivityCompat.shouldShowRequestPermissionRationale(activity,permission)){
                    isRationaleLaunched = true;
                    launchRationaleDialog(activity, ratinaleStrings.length==1 ? ratinaleStrings[0] : ratinaleStrings[index], permission, stringArrayList, permissions.length, appId);
                }else{
                    stringArrayList.add(permission);
                }
            }
            index++;
        }
        if(stringArrayList.size()>0){
            if(!shouldShowCustomRationaleDialog){
                launchPermissionFlowIfLastPermission(context, stringArrayList, true, appId);
            }
            bundle.putBoolean(IS_PERMISSION_GRANTED,false);
            bundle.putStringArrayList(PERMISSIONS_ARRAY,stringArrayList);
            return bundle;
        }
        bundle.putBoolean(IS_PERMISSION_GRANTED,!isRationaleLaunched);
        return bundle;
    }

    /**
     * launches the permission's flow if the last permission was checked and there are some permissions to be granted
     *
     * @param activity
     * @param stringArrayList
     * @param isLastPermission
     * @param appId
     */
    public static void launchPermissionFlowIfLastPermission(Context activity, ArrayList<String> stringArrayList, boolean isLastPermission, String appId){
        if(isLastPermission && stringArrayList.size()>0){
            activity.startActivity(new Intent(activity, RequestPermissionActivity.class)
                    .putExtra(RequestPermissionActivity.PERMISSIONS_ARRAY, stringArrayList.toArray(new String[stringArrayList.size()]))
                    .putExtra(RequestPermissionActivity.EXTRA_APPLICATION_ID,appId)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    /**
     *
     * @return true if the device installed is Android M, false otherwise
     */
    private static boolean isAndroidM(){
        return Build.VERSION.SDK_INT>=Build.VERSION_CODES.M;
    }

    /**
     * launches the rationale dialog where shows more context text over a dialog about the permission needed
     *
     * @param activity
     * @param rationale
     * @param permission
     * @param stringArrayList
     * @param appId
     */
    private static void launchRationaleDialog(final AppCompatActivity activity, final String rationale, final String permission, final ArrayList<String> stringArrayList, int permissionsCount, final String appId){
        if(materialDialogArrayList!=null && permissionsCount==materialDialogArrayList.size()){
            materialDialogArrayList.get(materialDialogArrayList.size()-1).show();
            return;
        }
        MaterialDialog materialDialog = new MaterialDialog.Builder(activity)
                .content(rationale)
                .title(R.string.hint_txt_dialog_title)
                .positiveText(R.string.hint_txt_dialog_allow)
                .negativeText(R.string.hint_txt_dialog_deny)
                .cancelable(false)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        stringArrayList.add(permission);
                        materialDialogArrayList.remove(materialDialogArrayList.size() - 1);
                        launchPermissionFlowIfLastPermission(activity, stringArrayList, materialDialogArrayList.size() == 0, appId);
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                        materialDialogArrayList.remove(materialDialogArrayList.size() - 1);
                        launchPermissionFlowIfLastPermission(activity, stringArrayList, materialDialogArrayList.size() == 0, appId);
                    }
                }).show();
        if(materialDialogArrayList==null){
            materialDialogArrayList = new ArrayList<MaterialDialog>();
        }
        materialDialogArrayList.add(materialDialog);
    }
}
