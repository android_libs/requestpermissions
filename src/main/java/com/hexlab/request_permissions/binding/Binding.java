package com.hexlab.request_permissions.binding;

import android.app.Activity;
import android.databinding.ObservableField;

/**
 * Created by oreste on 01/10/15.
 */
public class Binding {
    public void onClickListener() {
        activity.onBackPressed();
    }
    public ObservableField<String> text = new ObservableField<String>();
    private Activity activity;

    public Binding(Activity activity){
        this.activity = activity;
    }
}
